FROM openjdk:8-jdk-alpine
WORKDIR /opt
ADD target/product*.jar /opt/product-catalogue.jar
ADD product-catalogue.yml /opt/app-config.yml
EXPOSE 8020
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/opt/product-catalogue.jar", "server", "/opt/app-config.yml"]







